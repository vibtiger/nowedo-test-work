$(window).on('load', function() {
/**
 *	sticky nav
 */
	var menu_header_top = $( '.navbar', '#menu-header' ).offset().top,
		menu_header_heigth = $( '.navbar', '#menu-header' ).height();
		
	$(window).scroll(function() {
		if( $(this).scrollTop() > menu_header_top ) {
			$( '#menu-header' ).addClass( 'menu-sticky' );
			$( '.contn' ).css( 'padding-top', menu_header_heigth );
		} else {
			$( '#menu-header' ).removeClass( 'menu-sticky' );
			$( '.contn' ).css( 'padding-top', 0 );
		}
	});

/**
 *	header menu: item selection
 */
	$( '.nav-item', 'header' ).click( function() {
		$( '.nav-item', 'header' ).removeClass( 'active' );
		$(this).addClass( 'active' );
	});
	
/**
 *	button hover
 */
	var button_tiner;
	$( 'button.pink' ).hover( function() {
		let el$ = $( 'i', $(this) );
		button_tiner = setInterval( function() {
			let tmp1 = el$.eq(0).css( 'opacity' ),
				tmp2 = el$.eq(1).css( 'opacity' );				
			el$.eq(0).css( 'opacity', el$.eq(2).css( 'opacity' ) );
			el$.eq(1).css( 'opacity', tmp1 );
			el$.eq(2).css( 'opacity', tmp2 );

			tmp1 = el$.eq(4).css( 'opacity' );
			tmp2 = el$.eq(5).css( 'opacity' );
			el$.eq(3).css( 'opacity', tmp1 );
			el$.eq(4).css( 'opacity', tmp2 );
			el$.eq(5).css( 'opacity', el$.eq(2).css( 'opacity' ) );
		}, 250 );
		
	}, function() {
		let el$ = $( 'i', $(this) );
		clearInterval( button_tiner );
		el$.eq(0).css( 'opacity', 1 );
		el$.eq(1).css( 'opacity', 0.85 );
		el$.eq(2).css( 'opacity', 0.5 );
		el$.eq(3).css( 'opacity', 0.5 );
		el$.eq(4).css( 'opacity', 0.85 );
		el$.eq(5).css( 'opacity', 1 );
	});

/**
 *	button #go-next
 */
	$( '#go-next-sec', '#slid1'  ).click( function(){
		next_pos = $( $(this).data( 'nexts' ) ).offset().top;
		
		$('html, body').animate({
			scrollTop: next_pos
		}, 1000);
	});
	$( '#go-prev', '#slid1' ).click( function(){
		$('.carousel', '#slid1' ).carousel( 'prev' );
	});
	$( '#go-next', '#slid1' ).click( function(){
		$('.carousel', '#slid1' ).carousel( 'next' );
	});
	
/**
 *	#slid1 slaider
 */
	var slid2_stop = false;
	$(  '#slid2' ).hover(function(){
		slid2_stop = true;
	}, function(){
		slid2_stop = false;
	});
	$( '.arr-direct button', '#slid2'  ).click(function(){
		var el_this$ = '#' + $(this).attr( 'id' );
		slid2_stop = true;
		$( '.slid-item.slid-active', '#slid2' )
			.removeClass( 'slid-active fadeInLeft' )
			.addClass( 'fadeOutRight' );

		setTimeout(function(){
			$( '.slid-item.fadeOutRight', '#slid2' ).css({ display: 'none' });
			$( '.slid-wrap ' + el_this$ + '-item', '#slid2' )
				.css({ display: 'block' })
				.removeClass( 'fadeOutRight' )
				.addClass( 'fadeIn slid-active' );

			$( 'button.arrw', '#slid2' ).find( 'span' ).css( 'color', '#333333' );
			$( el_this$, '#slid2' ).find( 'span' ).css( 'color', '#fb5367' );


		}, 600);		
	});
	
	$( '.slid-wrap', '#slid2' ).css({
		height: $( '.slid-active' ).height() + 'px'
	});
	
	$( '.slid-item', '#slid2' ).addClass( 'animated' ).not( '.slid-active' ).addClass( 'fadeOutRight' );
	setInterval(function(){
		if ( !slid2_stop ) {
			let el$ = $( '.slid-item', '#slid2' ),
				ind = el$.index( $('.slid-active') ) + 1;
	
			if ( ind == el$.length ) {
				ind = 0;
			}
			$( '.slid-item.slid-active', '#slid2' )
				.removeClass( 'slid-active fadeInLeft fadeIn' )
				.addClass( 'fadeOutRight' );
	
			setTimeout(function(){
				$( '.slid-item.fadeOutRight', '#slid2' ).css({ display: 'none' });
				let ttt = el$.eq( ind )
					.css({ display: 'block' })
					.removeClass( 'fadeOutRight' )
					.addClass( 'fadeInLeft slid-active' ).attr( 'id' ).slice(0, -5);
					
					
				$( 'button.arrw', '#slid2' ).find( 'span' ).css( 'color', '#333333' );
				$( '#' + ttt, '#slid2' ).find( 'span' ).css( 'color', '#fb5367' );
			}, 600);
		}
	}, 3000);
});