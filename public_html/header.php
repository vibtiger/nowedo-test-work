<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=utf-8>
	<meta http-equiv= "X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>Nowedo</title>	
 	<meta name="description" content="">
	<meta name="keywords" content="">
	
	<link rel="shortcut icon" href="">
	<link rel="stylesheet" href= "../nowedo/css/animate.css">
	<link rel="stylesheet" href= "../nowedo/css/bootstrap.min.css">
	<link rel="stylesheet" href= "../nowedo/css/style.css">
	
</head>

<body>
	<header>
		<div class="container-fluid">
			<section id="top-header">
				<div class="container">
					<div class="row">
						<div class="ml-auto col-sm-12 col-md-auto">
							<p>
								<span class="br-span">Звоните</span>
								<span class="br-span">000-000-00-00</span>
							</p>
						</div>
						<div class="col-sm-12 col-md-auto">
							<p>
								<span class="br-span">Без выходных</span>
								<span class="br-span">с 9:00 до 23:00</span>
							</p>
						</div>
					</div>
				</div>
			</section>
			<section id="menu-header">
				<div class="container">
					<nav class="navbar navbar-toggleable-md navbar-light bg-light">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nowedo-naw" aria-controls="nowedo-naw" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="nowedo-naw">
							<ul class="navbar-nav">
								<li class="nav-item">
									<a class="nav-link" href="#">Головна</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link" href="#">Новини</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Про компанію</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Тренинги</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Тренери</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Статті</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Соціальні контакти</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Наші контакты</a>
								</li>
							</ul>
					  </div>
					</nav>
				</div>
			</section>
		</div>
	</header>
