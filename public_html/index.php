<?php
	require( 'header.php' );
?>
	<div class="container-fluid contn">
		<section id="slid1">
			<div class="container">
				<div class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="row">
								<div class="ml-auto col-sm-12 col-md-4 slid-txt">
									<div>
										<p class="p-h1">
											Персонал для детей
										</p>
										<p>
											Няни, гувернантки и репититоры для развития детей и спокойствия родителей.
										</p>
										<button class="btn pink">
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<span>Приступим</span>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>					
										</button>
									</div>
								</div>
								<div class="col-sm-12 col-md-7 slid-imeg">
									<img src="img/slide1_1.png" alt="slider1">				
								</div>
							</div>
	
						</div>
						<div class="carousel-item">
							<div class="row">
								<div class="ml-auto col-sm-12 col-md-4 slid-txt">
									<div>
										<p class="p-h1">
											Персонал для детей
										</p>
										<p>
											Няни, гувернантки и репититоры для развития детей и спокойствия родителей.
										</p>
										<button class="btn pink">
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<span>Приступим</span>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>					
										</button>
									</div>
								</div>
								<div class="col-sm-12 col-md-7 slid-imeg">
									<img src="img/slide1_1.png" alt="slider1">				
								</div>
							</div>
			
						</div>
						<div class="carousel-item">
							<div class="row slid-item">
								<div class="ml-auto col-sm-12 col-md-4 slid-txt">
									<div>
										<p class="p-h1">
											Персонал для детей
										</p>
										<p>
											Няни, гувернантки и репититоры для развития детей и спокойствия родителей.
										</p>
										<button class="btn pink">
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<span>Приступим</span>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>
											<i class="fas fa-circle"></i>					
										</button>
									</div>
								</div>
								<div class="col-sm-12 col-md-7 slid-imeg">
									<img src="img/slide1_1.png" alt="slider1">				
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row arr-direct">
					<div class="col-sm-12 offset-md-1 col-md-4">
						<button id="go-next-sec" class="btn arrw" data-nexts="#slid2">
							Узнать ещё
							<i class="fas fa-long-arrow-alt-down ripo"></i>
						</button>
						<div>
							<button id="go-prev" class="btn arrw">
								<i class="fas fa-long-arrow-alt-left lepo"></i>
								Пред
							</button>
							<button id="go-next" class="btn arrw">
								След
								<i class="fas fa-long-arrow-alt-right ripo"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /#slid1 -->
		
		<section id="slid2">
			<div class="container">
				<div class="row">
					<div class="ml-auto col-sm-12 col-md-11">
						<p class="p-h2">
							Родителям и детям
						</p>
					</div>
				</div>

				<div class="row arr-direct">
					<div class="col-sm-12 ml-auto col-md-5">
						<button id="go-why" class="btn arrw" data-nexts="#slid2">
							<i class="fas fa-long-arrow-alt-left lepo"></i>
							<span>Зачем?</span>
						</button>
						<button id="go-what" class="btn arrw">
							<span>Что?</span>
						</button>
						<button id="go-how" class="btn arrw">
							<span>Как?</span>
							<i class="fas fa-long-arrow-alt-right ripo"></i>
						</button>
					</div>
				</div>
				
				<div class="slid-wrap">
					<div id="go-why-item" class="slid-item slid-active">
						<div class="row">
							<div class="col-sm-12 col-md-6 slid-imeg">
								<img src="img/slide2_1.png" alt="slider2">				
							</div>
							<div class="ml-auto col-sm-12 col-md-5 slid-txt">
								<p class="p-h3">
									Зачем нанимают нянь?
								</p>
								<p class="bullet-pink">
									Мамам и папам нужна помощница, которая заберёт ребёнка из детского садика или школы домой, поможет ему с домашним заданием, посещением спортивных занятий и прочими важными детскими делами.
								</p>
								<p class="bullet-yellow">
									Ребёнку требуется хороший педагог, который подготовит его к учебному году или экзаменам.
								</p>
								<p class="bullet-pink">
									Всей семье необходима поддержка чудесной Мэрри Поппинс, сочетающей в себе безукоризненную дисциплину и искреннюю любовь к детям.
								</p>
								<button class="btn pink">
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<span>Узнать больше</span>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>					
								</button>
							</div>
						</div>
					</div>
					<div id="go-what-item" class="slid-item">
						<div class="row">
							<div class="col-sm-12 col-md-6 slid-imeg">
								<img src="img/slide2_1.png" alt="slider2">				
							</div>
							<div class="ml-auto col-sm-12 col-md-5 slid-txt">
								<p class="p-h3">
									Что делает няня?
								</p>
								<p class="bullet-pink">
									Мамам и папам нужна помощница, которая заберёт ребёнка из детского садика или школы домой, поможет ему с домашним заданием, посещением спортивных занятий и прочими важными детскими делами.
								</p>
								<p class="bullet-yellow">
									Ребёнку требуется хороший педагог, который подготовит его к учебному году или экзаменам.
								</p>
								<p class="bullet-pink">
									Всей семье необходима поддержка чудесной Мэрри Поппинс, сочетающей в себе безукоризненную дисциплину и искреннюю любовь к детям.
								</p>
								<button class="btn pink">
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<span>Узнать больше</span>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>					
								</button>
							</div>
						</div>
					</div>
					<div id="go-how-item" class="slid-item">
						<div class="row">
							<div class="col-sm-12 col-md-6 slid-imeg">
								<img src="img/slide2_1.png" alt="slider2">				
							</div>
							<div class="ml-auto col-sm-12 col-md-5 slid-txt">
								<p class="p-h3">
									Как нанять няню?
								</p>
								<p class="bullet-pink">
									Мамам и папам нужна помощница, которая заберёт ребёнка из детского садика или школы домой, поможет ему с домашним заданием, посещением спортивных занятий и прочими важными детскими делами.
								</p>
								<p class="bullet-yellow">
									Ребёнку требуется хороший педагог, который подготовит его к учебному году или экзаменам.
								</p>
								<p class="bullet-pink">
									Всей семье необходима поддержка чудесной Мэрри Поппинс, сочетающей в себе безукоризненную дисциплину и искреннюю любовь к детям.
								</p>
								<button class="btn pink">
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<span>Узнать больше</span>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>
									<i class="fas fa-circle"></i>					
								</button>
							</div>
						</div>
					</div>
				</div>


				
				
			</div>			
		</section>
		<!-- /#slid2 -->
	</div>
	<!-- /.container-fluid .cont -->

<?php
	require( 'footer.php' );
?>
