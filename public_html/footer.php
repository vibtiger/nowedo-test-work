	<footer>
	</footer>


<!-- MODAL -->
	<!-- Send letter -->
	<div class="modal" id="modal-letter" tabindex="-1" role="dialog" aria-labelledby="modal-letter-label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title item-titl" id="modal-letter-label">Send letter</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="leter-name">Name</label>
							<input type="text" class="form-control" id="leter-name" name="name">
						</div>
						<div class="form-group">
							<label for="leter-email">Email</label>
							<input type="email" class="form-control" id="leter-email" name="email" placeholder="example@email.ex">
						</div>
						<div class="form-group">
							<label for="letter-text">Example textarea</label>
							<textarea class="form-control" id="letter-text" name="message" rows="3"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn">Submit</button>
						</div>
					</form>					
				</div>
			</div>
		</div>
	</div>

	<!-- SMS online -->
	<div id="sec-send" class="modal" tabindex="-1" role="dialog" aria-labelledby="ss-label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title item-titl" id="ss-label"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p></p>
				</div>
			</div>
		</div>
	</div>

	<script src="../nowedo/js/jquery-3.3.1.min.js"></script>
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script src="../nowedo/js/bootstrap.min.js"></script>
	<script src="../nowedo/js/custom.js"></script>
</body>
</html>
